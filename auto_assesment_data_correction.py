import psycopg2
from psycopg2 import Error
connection = psycopg2.connect(
        user="postgres",
        password="root",
        host="localhost",
        port="5432",
        database="research1706"
)
cursor = connection.cursor()
#getting dict of plans and thier goals
query="select plan_id, goal_id from core.goals_in_plan order by plan_id, goal_id, priority"
cursor.execute(query)
rows = cursor.fetchall()
goals={}
for key, value in rows:
    if key in goals:
        goals[key].append(value)
    else:
        goals[key] = [value]
#getting dict of goals and thier tasks
query="select goal_id, task_id from config.tasks_in_goal order by goal_id, sequence"
cursor.execute(query)
rows = cursor.fetchall()
tasks={}
for key, value in rows:
    if key in tasks:
        tasks[key].append(value)
    else:
        tasks[key] = [value]
#getting dict of tasks and their modalities
query="select task_id, m.id from config.modalities_in_tasks m join config.lookup_values l on l.id=m.modality_id where l.value<>'Home Based Activity' order by task_id, l.sequence"
cursor.execute(query)
rows = cursor.fetchall()
modalities={}
for key, value in rows:
    if key in modalities:
        modalities[key].append(value)
    else:
        modalities[key] = [value]
#getting activities data order by plan and goal id
def getActivities(plan_id, goal_id):
    query = "select activity_id, plan_id, goal_id, task_id,modality_in_task_id, level, cast(score as float), cast(task_attempt_number as int) from logging.activity_summary where mode_id = (select id from config.lookup_values where value='Plan') and plan_id='"+str(plan_id)+"' and goal_id='"+str(goal_id)+"' and plan_id in (select id from core.learning_plans where stopped_on is null) order by plan_id, goal_id, started_on"
    cursor.execute(query)
    rows = cursor.fetchall()
    activities=[]
    for row in rows:
        activities.append(list(row))
    return activities
prev_plan_id=''
prev_goal_id=''
i=0
n=[]
un=[]
def function(activities):
    prev_plan_id=''
    prev_goal_id=''
    i=0
    while (i<(len(activities))):
        if prev_plan_id!=activities[i][1] or prev_goal_id!=activities[i][2]:
            prev_plan_id=activities[i][1]
            prev_goal_id=activities[i][2]
            no_of_tasks=len(tasks[activities[i][2]])
            levels=3
            levelf=3
            task_index=0
            task_index2=0
            l=[]
            locked=[]
            task_index3=0
            l1=0
            a=[]
            for r in range(no_of_tasks):
                a.append([1]*3)
            l=list(range(1,no_of_tasks+1))+[0,0]
            locked=[0]*no_of_tasks
            j=0
            '''
            while (j<i):
                print("i=",i)
                print(activities)
                un.append(activities[i][0])
                del activities[j]
                j=j+1
            i=0'''
        if (len(l)>2):
            #res=int(input("G-"+str(i+1)+"\tT-"+str(l[task_index])+"\tA"+str(a)+"\tL-"+str(levels)+"\t"))
            res=0
            no_of_modalities=len(modalities[activities[i][3]])-1
            #print(modalities[activities[i][3]])
            for j in modalities[activities[i][3]]:
                k=0
                found=0
                while (k<len(activities) and i<len(activities)):
                    '''if (activities[i][1]!=activities[k][1] or activities[i][2]!=activities[k][2]):
                        print(activities[i][1], activities[k][1])
                        print(activities[i][2], activities[k][2])
                        m=0
                        while (m<k):
                            un.append(activities[m][0])
                            m=m+1
                        i=k
                        break'''
                    if (tasks[activities[i][2]][task_index2]==activities[k][3] and j==activities[k][4] and levels==activities[k][5]):
                        #print(j)
                        #print(modalities[activities[i][3]])
                        if (modalities[activities[i][3]].index(j)!=1):
                            res=res+activities[k][6]
                        n.append(activities[k][0])
                        #print("\tT-"+str(l[task_index])+"\tA"+str(a[task_index][levels-1])+"\tL-"+str(levels)+"\t")
                        found=1
                        del activities[k]
                        break
                    k=k+1
                if (found==0):
                    break
            i=k
            #print(un,n)
            res=res/no_of_modalities
            #print("res="+str(res))
            if (levels==0):
                levels=1
                a[task_index][levels-1]=a[task_index][levels-1]+1
                l[-1]=(l[-1]+1)
                locked[task_index]=levels
                l1=1
                continue
            if (res<=0.5):
                l[-1]=(l[-1]+1)
            else:
                l[-2]=l[-2]+1
                l[-1]=0
            if (l[-2]==3):
                #print("success")
                #print (i)
                if (locked[task_index]<levels and levels!=3):
                    locked[task_index]=levels
                if (levels==3):
                    l[-2]=0
                    del l[task_index]
                    task_index2=task_index2+1
                    del locked[task_index]
                    del a[task_index]
                    task_index3=task_index3+1
                    l[-1]=0
                else:
                    if (i==0):
                        levelf=levels+1
                    task_index=task_index+1
                    task_index2=task_index2+1
                    if (task_index>=(len(l)-2)):
                        task_index=0
                        task_index2=task_index3
                        #levels=3
                        levels=locked[task_index]+1
                        l1=1
                    l[-2]=0
                    l[-1]=0
                #a[task_index][levels-1]=1
            elif (l[-1]==2 and not(l1==1 and levels-1<=locked[task_index])):
                levels=levels-1
                if (levels!=0):
                    l[-2]=0
                    l[-1]=0
                a[task_index][levels-1]=1
            elif (l[-1]==5):
                #print("Failed")
                l[-1]=0
                a[task_index][levels-1]=a[task_index][levels-1]+1
            else:
                a[task_index][levels-1]=a[task_index][levels-1]+1
    for p in activities:
        if p[0] not in n:
            print('\''+str(p[0])+'\',')
print(goals)
for i in goals:
    '''
    if i!='4532b098-273c-4b91-9bfa-678f74207332':
        continue
    '''
    print('********************************************')
    print(i)
    try:
        for j in goals[i]:
            activities=getActivities(i,j)
            function(activities)
    except:
        print("--------------------------------------------------------------------------------------------------------------")
        '''
    for j in goals[i]:
        activities=getActivities(i,j)
        function(activities) '''
