Folder containing scripts for generic tasks

Filename:Functionality

Make sure to change the directory path with forward_slash('/') not backward_slash('\')
Install the below packages before running these scripts
    - cv2       --> pip install opencv-python
    - numpy     --> pip install numpy
    - pandas    --> pip install pandas
    - openpyxl  --> pip install openpyxl
    - json      --> pip install jsonlib
    - shutil    --> pip install pytest-shutil
    
Steps to install the packages in the Python IDLE
    - open command prompt
    - move the directory to the python scripts ('C:\Users\{username}\AppData\Local\Programs\Python\Python311\Scripts')
    - Then run - pip install pandas

AV JSON.py
    Creates the JSONS for the AV 
    Usage: Download the Excel sheet into your local directory and change the file variable with that path.
           Rename the column names with the names as mentioned in the my_list [Task_ID, cuepoint, questionid, Question, value_01, value_02, value_03, Prompt, Type]
           Run the python script then JSON file will be saved as AV.json where the current script is saved.
Chatbot JSON.PY
    Created the JSONS for the Chatbot
    Usage: Download the Excel sheet into your local directory and change the file variable with that path.
           Rename the column names with the names as mentioned in the my_list [Task_ID, cprompt, answer, cquestion, cquestionid, display_type]
           Run the python script then JSON file will be saved as chatbot.json where the current script is saved.

chatbot folder creation.py
    It creates the chatbot directory ex:BT001/chatbot/assets/default/question1
    Usage: Change the parent_dir with the path where the all tasks folders were created. The chatbot folder will be created in the parent_dir

print files.py
    It displays all the files in the given task folder
    Usage: Change the parent_directory variable to the path of the task directory

png to jpg.py
    It converts all png to jpg files in AV and chatbot. Converted images will be saved in the same directory.
    Usage: Change the parent_directory to the path where we want to convert the pngs

copy AV.py
    It copies only AV assets from source folder to destination.
    Usage: Change the target variable with your destination folder where you want to copy the files
           Change the path variable with the source path ("//RAJ/task/")

copy chatbot.py
    It copies only chatbot assets from source folder to destination
    Usage: Change the target variable with your destination folder where you want to copy the files
           Change the path variable with the source path ("//RAJ/task/")
           
copy obj.py
    It copies the objective vedios to the target folder in specified location
    Usage: Change the source path and destination path as required

rename video files.py
    Renaming the AV,OBJ,HBA videos to the required format
    For ex: AV - BT001.mp4
            HBA- BT001HBA.mp4
            OBJ- BT001OBJ.mp4
    Usage: Change the parent_directory variable with the path where we neeed to rename the files

directory creation dynamic.py
    It creates the AV task folder structure dynmically as per the excel sheet.
    Usage: Change the file variable with the path of the excel sheet
           Change the Task_ID with the name that is mentioned in excel sheet
           Change the parent_dir where folders needs to be created.

directory creation static.py
    It creates the AV task folder structure .
    Usage: Change 'n' to number of files you need to create
           Change taskname to task name ['BT' or 'ST' or 'SE' or 'OT']
           Change the parent_dir to the destination path





        
