# importing required packages
from pathlib import Path
import shutil
import os
#Path where the objective videos located
src = '//RAJ/se-obj'
#Target Path
trg = '//RAJ/task/'
files=os.listdir(src)
for fname in files:
        if (int(fname[7:10])<51):
                continue
        t=trg+fname[:2]+fname[7:10]+"/"+"OBJ"
	# copying the files to the
	# destination directory
        print(t)
        shutil.copy2(os.path.join(src,fname), t)
